package com.bancolombia.APM_SUFI.definition;

import com.bancolombia.APM_SUFI.steps.LoginSteps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class LoginDefinition {

	@Steps
	LoginSteps loginStep;

	@Given("^que estoy en la aplicacion de sufi$")
	public void que_estoy_en_la_aplicacion_de_sufi() throws Throwable {

		loginStep.abrirApp();
	}

	@When("^inicio sesion con usuario \"([^\"]*)\" y contrasena \"([^\"]*)\"$")
	public void inicio_sesion_con_usuario_y_contrasena(String Usuario, String Password) throws Throwable {

	}

	@When("^verifico login correcto$")
	public void verifico_login_correcto() throws Throwable {

	}

	@Then("^doy clic en simular$")
	public void doy_clic_en_simular() throws Throwable {

	}
}
