package com.bancolombia.APM_SUFI.pages;

import org.hamcrest.MatcherAssert;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://www.grupobancolombia.com/wps/portal/personas")
public class LoginPages extends PageObject {

	@FindBy(xpath = "//*[contains(@id, 'wtUserNameInput')]")
	private WebElementFacade txtUsuario;

	@FindBy(xpath = "//*[contains(@id, 'wtPasswordInput')]")
	private WebElementFacade txtPassword;

	@FindBy(xpath = "//*[contains(@id, 'wt34')]")
	private WebElementFacade btnIngreso;

	@FindBy(xpath = "//*[contains(@href, '/om_apm_consumer/Application_Edit.jsf')]")
	private WebElementFacade simular;

	public void login(String Usuario, String Password) {
		txtUsuario.sendKeys(Usuario);
		txtPassword.sendKeys(Password);
		btnIngreso.click();

	}

	public void validarSimular() {
		MatcherAssert.assertThat("no inicio sesion", simular.isDisplayed());
		simular.click();
	}

}
