package com.bancolombia.APM_SUFI.steps;

import org.fluentlenium.core.annotation.Page;

import com.bancolombia.APM_SUFI.pages.LoginPages;

import net.thucydides.core.annotations.Step;

public class LoginSteps {

	@Page
	LoginPages login;

	@Step
	public void abrirApp() {
		login.open();
	}

	@Step
	public void inicioSesion(String Usuario, String Password) {
		login.login(Usuario, Password);

	}

	@Step
	public void validarIngresoASimular() {
		login.validarSimular();

	}
}
